const AWS = require('aws-sdk');

// Replace this with the appropriate AWS SDK for your chosen database
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const AWS = require('aws-sdk');
const s3 = new AWS.S3();

const tableName = "chat-sls-video-streaming-table";

// Replace this with the appropriate AWS SDK for your chosen database

exports.chooseVideo = async (event, context) => {
  const bucketName = 'my-media-bucket';
  const prefix = 'media/';

  // List the objects in the media directory
  const objects = await s3.listObjects({ Bucket: bucketName, Prefix: prefix }).promise();

  // Choose a random video from the list of objects
  const videos = objects.Contents.filter(object => object.Key.endsWith('.mp4'));
  const video = videos[Math.floor(Math.random() * videos.length)];
  const videoUrl = `https://${bucketName}.s3.amazonaws.com/${video.Key}`;

  // Store the chosen video and timestamp in the database
  const timestamp = Date.now();
  await dynamoDb.put({
    TableName: tableName,
    Item: {
      id: 'current-video',
      videoUrl: videoUrl,
      timestamp: timestamp,
    },
  }).promise();

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Video chosen successfully',
    }),
  };
};



exports.getVideo = async (event, context) => {
  // Retrieve the current video and timestamp from the database
  const result = await dynamoDb.get({
    TableName: tableName,
    Key: {
      id: 'current-video',
    },
  }).promise();

  const videoUrl = result.Item.videoUrl;
  const timestamp = result.Item.timestamp;

  return {
    statusCode: 200,
    body: JSON.stringify({
      videoUrl: videoUrl,
      timestamp: timestamp,
    }),
  };
};
